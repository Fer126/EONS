package Interfaces;
import EON.Utilitarios.*;
import EON.*;
import EON.Algoritmos.*;
import java.io.IOException;
import java.util.ArrayList;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import org.jfree.chart.*;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.*;




/**
 *
 * @author Team Delvalle
 * Frame que se encargad de la interfaz con el usurio y realizar la simulacion de una Red Optica Elastica.
 * Permite realizar una simulacion teniendo:
 * - Una topologia
 * - Un conjunto de algoritmos(para esta version dos).
 * - Un tipo de demanda que sera generada. 
 * Para esta version las simulaciones se realizan considerando para cada demanda se tienen el mismo valor
 * en todas las demandas para el tiempo de permanencia en la red como para la cantidad de FSs requeridos.
 */
public class VentanaPrincipal extends javax.swing.JFrame {

    private Topologias Redes; // topologias disponibles
    private double factorTiempo; // factor de tiempo indicado por el usuario
    private double factorCapacidad; // cantidad de FSs que contendra cada demanda
    private int capacidadPorEnlace; // cantidad de FSs por enlace en la topologia elegida
    private int tiempoTotal; // Iiempo que dura una simualcion
    private double anchoFS; // ancho de un FS en los enlaces
    private int cantidadRedes; //cantidad de redes exitentes en el Simulador
    
    public VentanaPrincipal() {
        initComponents();
        /* Inicialmente no se muestran en panel para editar las caracteristicas de los enlaces
        */
        this.panelCambiarDatosdelosEnlaces.setVisible(false);
        this.botonCambiarDatosdelosEnlaces.setEnabled(false);
        this.PaneleEditarEnalce.setVisible(false);
        /*
        */
        this.Redes=new Topologias(); // asignamos todas las topologias disponibles
        //obtenemos el factor de tiempo inidicado por el usuario
        this.factorTiempo=Double.parseDouble(this.spinnerFactorTiempo.getValue().toString()); 
        //obtenemos la cantidad de FS que tendra cada demanda indicado por el usuario
        this.factorCapacidad=Double.parseDouble(this.spinnerFactorCapacidad.getValue().toString());
        //obtenemos la cantidad de FS de los enlaces indicados por el usuario
        this.capacidadPorEnlace=Integer.parseInt(this.spinnerEspectroporEnlace.getValue().toString());
        //Tiempo de simulacion indicado por el usuario
        this.tiempoTotal=Integer.parseInt(this.spinnerTiempoSimulacion.getValue().toString());
        // ancho de los FSs de la toplogia elegida, tambien indicado por el usuario
        this.anchoFS=Double.parseDouble(this.spinnerAnchoFS.getValue().toString());
        /*No mostramos inicialmente los paneles que muestran los Resultados
        */
        this.etiquetaResultado.setVisible(false);
        this.panelResultado.setVisible(false);
        this.setTitle("EON Simulator");
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listaRedes = new javax.swing.JList<String>();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaRSA = new javax.swing.JList<String>();
        jScrollPane3 = new javax.swing.JScrollPane();
        listaDemandas = new javax.swing.JList<String>();
        botonEjecutarSimulacion = new javax.swing.JButton();
        etiquetaTopologia = new javax.swing.JLabel();
        etiquetaRSA = new javax.swing.JLabel();
        etiquetaDemanda = new javax.swing.JLabel();
        etiquetaError = new javax.swing.JLabel();
        panelCambiarDatosdelosEnlaces = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        listaEnlaces = new javax.swing.JList<String>();
        PaneleEditarEnalce = new javax.swing.JPanel();
        bGuardar = new javax.swing.JButton();
        campoDeTextoEspectro = new javax.swing.JTextField();
        campoDeTextoAnchoFS = new javax.swing.JTextField();
        etiquetaEspectro = new javax.swing.JLabel();
        etiquetaAnchoFS = new javax.swing.JLabel();
        etiquetaEditarEnlace = new javax.swing.JLabel();
        etiquetaErrorEditarEnlace = new javax.swing.JLabel();
        botonCerrarEditarEnlace = new javax.swing.JButton();
        botonCerrarCerrarDatosEnlaces = new javax.swing.JButton();
        etiquetaTituloDeEnlaces = new javax.swing.JLabel();
        botonCambiarDatosdelosEnlaces = new javax.swing.JButton();
        etiquetaCapacidadActual = new javax.swing.JLabel();
        etiquetaTiempoActual = new javax.swing.JLabel();
        etiquetaAnchoFSActual = new javax.swing.JLabel();
        etiquetaFactorTiempo = new javax.swing.JLabel();
        spinnerTiempoSimulacion = new javax.swing.JSpinner();
        spinnerEspectroporEnlace = new javax.swing.JSpinner();
        spinnerAnchoFS = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        spinnerFactorCapacidad = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        spinnerFactorTiempo = new javax.swing.JSpinner();
        etiquetaResultado = new javax.swing.JLabel();
        etiquetaImagen = new javax.swing.JLabel();
        panelResultado = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        etiqueta1 = new javax.swing.JLabel();
        etiqueta2 = new javax.swing.JLabel();
        etiquetaTextoDemandasTotales = new javax.swing.JLabel();
        etiquetaDemandasTotales = new javax.swing.JLabel();
        etiquetaGHz = new javax.swing.JLabel();
        etiquetaValorGHz = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        spinnerEarlang = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        spinnerPaso = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        listaRedes.setModel(new javax.swing.AbstractListModel() {
            String[] strings = {"Red 1","Red 2"};
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaRedes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaRedesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(listaRedes);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 112, 58));

        listaRSA.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "FAR - FF", "FAR - LF", "FAR - RF", "FAR - EF", "FAR - LU", "FAR - MU" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listaRSA.setToolTipText("");
        jScrollPane2.setViewportView(listaRSA);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 112, 100));

        listaDemandas.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Tiempo de permanecia y FSs Fijos" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(listaDemandas);
        listaDemandas.getAccessibleContext().setAccessibleName("");
        listaDemandas.getAccessibleContext().setAccessibleDescription("");

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, -1, 73));

        botonEjecutarSimulacion.setText("Simular");
        botonEjecutarSimulacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEjecutarSimulacionActionPerformed(evt);
            }
        });
        getContentPane().add(botonEjecutarSimulacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 480, 111, 46));

        etiquetaTopologia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaTopologia.setText("Topologia");
        getContentPane().add(etiquetaTopologia, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, -1, -1));

        etiquetaRSA.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaRSA.setText("Algoritmo RSA");
        getContentPane().add(etiquetaRSA, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, -1));

        etiquetaDemanda.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaDemanda.setText("Demanda");
        getContentPane().add(etiquetaDemanda, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, -1, -1));
        getContentPane().add(etiquetaError, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 580, 394, 23));

        panelCambiarDatosdelosEnlaces.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        panelCambiarDatosdelosEnlaces.setEnabled(false);

        listaEnlaces.setToolTipText("");
        listaEnlaces.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaEnlacesMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(listaEnlaces);

        bGuardar.setText("Guardar");
        bGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bGuardarActionPerformed(evt);
            }
        });

        botonCerrarEditarEnlace.setText("Cerrar");
        botonCerrarEditarEnlace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarEditarEnlaceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PaneleEditarEnalceLayout = new javax.swing.GroupLayout(PaneleEditarEnalce);
        PaneleEditarEnalce.setLayout(PaneleEditarEnalceLayout);
        PaneleEditarEnalceLayout.setHorizontalGroup(
            PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaneleEditarEnalceLayout.createSequentialGroup()
                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PaneleEditarEnalceLayout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(etiquetaEditarEnlace, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PaneleEditarEnalceLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PaneleEditarEnalceLayout.createSequentialGroup()
                                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(etiquetaAnchoFS, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                                    .addComponent(etiquetaEspectro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(campoDeTextoEspectro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(campoDeTextoAnchoFS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(PaneleEditarEnalceLayout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(bGuardar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(botonCerrarEditarEnlace))))
                    .addComponent(etiquetaErrorEditarEnlace, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PaneleEditarEnalceLayout.setVerticalGroup(
            PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PaneleEditarEnalceLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaEditarEnlace, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(campoDeTextoEspectro)
                    .addComponent(etiquetaEspectro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(campoDeTextoAnchoFS)
                    .addComponent(etiquetaAnchoFS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(PaneleEditarEnalceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bGuardar)
                    .addComponent(botonCerrarEditarEnlace))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(etiquetaErrorEditarEnlace, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        botonCerrarCerrarDatosEnlaces.setText("Cerrar");
        botonCerrarCerrarDatosEnlaces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarCerrarDatosEnlacesActionPerformed(evt);
            }
        });

        etiquetaTituloDeEnlaces.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaTituloDeEnlaces.setText("Enlaces");

        javax.swing.GroupLayout panelCambiarDatosdelosEnlacesLayout = new javax.swing.GroupLayout(panelCambiarDatosdelosEnlaces);
        panelCambiarDatosdelosEnlaces.setLayout(panelCambiarDatosdelosEnlacesLayout);
        panelCambiarDatosdelosEnlacesLayout.setHorizontalGroup(
            panelCambiarDatosdelosEnlacesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                .addGroup(panelCambiarDatosdelosEnlacesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(etiquetaTituloDeEnlaces))
                    .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                        .addGroup(panelCambiarDatosdelosEnlacesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(botonCerrarCerrarDatosEnlaces)))
                        .addGap(18, 18, 18)
                        .addComponent(PaneleEditarEnalce, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelCambiarDatosdelosEnlacesLayout.setVerticalGroup(
            panelCambiarDatosdelosEnlacesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaTituloDeEnlaces, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCambiarDatosdelosEnlacesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonCerrarCerrarDatosEnlaces))
                    .addGroup(panelCambiarDatosdelosEnlacesLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(PaneleEditarEnalce, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        getContentPane().add(panelCambiarDatosdelosEnlaces, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 30, -1, 250));

        botonCambiarDatosdelosEnlaces.setText("Cambiar Datos de los Enlaces");
        botonCambiarDatosdelosEnlaces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCambiarDatosdelosEnlacesActionPerformed(evt);
            }
        });
        getContentPane().add(botonCambiarDatosdelosEnlaces, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, 20));

        etiquetaCapacidadActual.setText("Capacidad");
        getContentPane().add(etiquetaCapacidadActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 110, 20));

        etiquetaTiempoActual.setText("Tiempo de Simulacion");
        getContentPane().add(etiquetaTiempoActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, 110, 20));

        etiquetaAnchoFSActual.setText("Ancho FS");
        getContentPane().add(etiquetaAnchoFSActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 110, 20));

        etiquetaFactorTiempo.setText("Tiempo de Pemancia");
        getContentPane().add(etiquetaFactorTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, 110, 20));

        spinnerTiempoSimulacion.setModel(new javax.swing.SpinnerNumberModel(100, 50, 100000, 25));
        getContentPane().add(spinnerTiempoSimulacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, 20));

        spinnerEspectroporEnlace.setModel(new javax.swing.SpinnerNumberModel(50, 5, 100000, 25));
        getContentPane().add(spinnerEspectroporEnlace, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, -1, 20));

        spinnerAnchoFS.setModel(new javax.swing.SpinnerNumberModel(2.0d, 2.0d, 100000.0d, 2.0d));
        getContentPane().add(spinnerAnchoFS, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 50, -1, 20));

        jLabel1.setText("FSs por demanda");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, 100, 20));

        spinnerFactorCapacidad.setModel(new javax.swing.SpinnerNumberModel(2, 2, 12, 1));
        spinnerFactorCapacidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerFactorCapacidadStateChanged(evt);
            }
        });
        getContentPane().add(spinnerFactorCapacidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 420, 70, 20));

        jLabel2.setText("FSs por Enlace");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, -1, 20));

        jLabel3.setText("GHz");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 50, 30, 20));

        spinnerFactorTiempo.setModel(new javax.swing.SpinnerNumberModel(0.1d, 0.001d, 0.9d, 0.01d));
        getContentPane().add(spinnerFactorTiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 390, 70, 20));

        etiquetaResultado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaResultado.setText("Resultado de la Simulación");
        getContentPane().add(etiquetaResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 290, 180, 20));
        getContentPane().add(etiquetaImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 100, 320, 170));

        panelResultado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout panelResultadoLayout = new javax.swing.GroupLayout(panelResultado);
        panelResultado.setLayout(panelResultadoLayout);
        panelResultadoLayout.setHorizontalGroup(
            panelResultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 466, Short.MAX_VALUE)
        );
        panelResultadoLayout.setVerticalGroup(
            panelResultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 276, Short.MAX_VALUE)
        );

        getContentPane().add(panelResultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 320, 470, 280));

        jLabel5.setText("Unidades");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, -1, 20));

        etiqueta1.setText("Del tiempo de Simulacion");
        getContentPane().add(etiqueta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 390, -1, 20));

        etiqueta2.setText(" FSs");
        getContentPane().add(etiqueta2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 420, -1, 20));

        etiquetaTextoDemandasTotales.setText("Cantidad total de Demandas");
        getContentPane().add(etiquetaTextoDemandasTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 320, 140, 20));
        getContentPane().add(etiquetaDemandasTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 340, 90, 30));

        etiquetaGHz.setText("GHz");
        getContentPane().add(etiquetaGHz, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 420, 30, 20));
        getContentPane().add(etiquetaValorGHz, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 420, 40, 20));

        jLabel4.setText("Carga de Trafico Maximo");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 70, 120, 20));

        spinnerEarlang.setModel(new javax.swing.SpinnerNumberModel(500, 500, 1500, 100));
        getContentPane().add(spinnerEarlang, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 70, 80, -1));

        jLabel6.setText("Earlang");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, 40, 20));

        jLabel7.setText("Paso (u)");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, -1, -1));

        spinnerPaso.setModel(new javax.swing.SpinnerNumberModel(10, 1, 100, 10));
        getContentPane().add(spinnerPaso, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 70, -1, -1));

        jLabel8.setText("Earlang");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 70, 40, 20));

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void botonEjecutarSimulacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEjecutarSimulacionActionPerformed
        
        /* Al inicio de cada Simulacion e+condemos los paneles de Resultado
        */
        this.etiquetaResultado.setVisible(false);
        this.panelResultado.setVisible(false);
        
        
        GrafoMatriz G [] = new GrafoMatriz[8]; // Se tiene una matriz de adyacencia por algoritmo RSA en el Simulador
        
        Demanda d= new Demanda();  // Demanda a recibir
        Resultado r=new Resultado(); // resultado obtenido en una demanda. Si r==null se cuenta como bloqueo
        String mensajeError = "Seleccione: "; // mensaje de error a mostrar eal usuario si no ha completado los parametros de
                                               // simulacion
        
        List<String> RSA = new LinkedList<String>(); // lista de Algoritmos RSA seleccionados
        
        
        int E=(int)this.spinnerEarlang.getValue(); // se obtiene el limite de carga (Erlang) de trafico seleccionado por el usuario
        int demandasPorUnidadTiempo=0; //demandas por unidad de tiempo. Considerando el punt decimal
        int earlang=0; //Carga de trafico en cada simulacion
        int k=-1; // contador auxiliar
        int paso=(int)this.spinnerPaso.getValue(); // siguiente carga de trafico a simular (Erlang)
        int contD=0; // contador de demandas totales
        int tiempoT=Integer.parseInt(this.spinnerTiempoSimulacion.getValue().toString()); // Tiempo de simulacion especificada por el usaurio
        int capacidadE=Integer.parseInt(this.spinnerEspectroporEnlace.getValue().toString()); // espectro por enalce
        double anchoFS=Double.parseDouble(this.spinnerAnchoFS.getValue().toString()); // ancho de FS
        //factor del tiempo de simulacion especificado por el usuario
        int t=(int)(Double.parseDouble(this.spinnerFactorTiempo.getValue().toString())*tiempoT);
        //cantidad de FS por demanda expresado pro el usuario
        int b=(int)(Double.parseDouble(this.spinnerFactorCapacidad.getValue().toString()));
        
        if(this.listaDemandas.getSelectedIndex()>=0 && listaRSA.getSelectedIndex()>=0 && 
                this.listaRedes.getSelectedIndex()>=0){ // si todos los parametros fueron seleccionados
           this.etiquetaError.setVisible(true); // habilitamos la etiqueta de error
           
            RSA= this.listaRSA.getSelectedValuesList(); // obtenemos los algoritmos RSA seleccionados
            String redSeleccionada = this.listaRedes.getSelectedValue(); // obtenemos la topologia seleccionada
            String demandaSeleccionada = this.listaDemandas.getSelectedValue(); // obtenemos el tipo de trafico seleccionado
           
            int [] contB=new int [RSA.size()]; // array encargado de almacenar en cada posicion la cantidad de bloqueo para cada
                                                // algoritmo seleccionado
            List prob []= new LinkedList[RSA.size()]; // probabilidad de bloqueo para cada algoritmo RSA selecciondo 
            
            
            for(int i=0;i<prob.length;i++){
                prob[i]=new LinkedList(); // para cada algoritmo, se tiene una lista enlazada que almacenara la Pb 
                                            // obtenidad en cada simulacion
            } 
            
            switch(redSeleccionada){ // cagamos los datos en las matrices de adyacencia segun la topologia seleccionada
                case "Red 0":
                    for(int i=0;i<8;i++){
                        G[i]=new GrafoMatriz(this.Redes.getRed(0).getCantidadDeVertices());
                        G[i].insertarDatos(this.Redes.getTopologia(0));
                    }
                    break;
                case "Red 1":
                    for(int i=0;i<8;i++){
                        G[i]=new GrafoMatriz(this.Redes.getRed(1).getCantidadDeVertices());
                        G[i].insertarDatos(this.Redes.getTopologia(1));
                    }
                    break;
                case "Red 2":
                    for(int i=0;i<8;i++){
                        G[i]=new GrafoMatriz(this.Redes.getRed(2).getCantidadDeVertices());
                        G[i].insertarDatos(this.Redes.getTopologia(2));
                    }
            }
           while(earlang<=E){ // mientras no se llega a la cargad de trafico maxima
                  for(int i=0;i<tiempoT;i++){ // para cada unidad de tiempo
                   demandasPorUnidadTiempo = Utilitarios.demandasTotalesPorTiempo(earlang, t); // obtener las demandas
                                                                                               // por unidad de tiempo
                     for(int j=0;j<demandasPorUnidadTiempo;j++){ // para cada demanda
                        switch(demandaSeleccionada){ // se ve que modelo de trafico fue seleccionado
                        case "Tiempo de permanecia y FSs Fijos":
                             d.obtenerDemandas(b, t,G[0].getCantidadDeVertices()-1);  // obtenemos un origen, fin, catidad de FS y teimpo de permanecia
                             break;
                        } 
                        ListaEnlazada[] ksp=Utilitarios.KSP(G[0], d.getOrigen(), d.getDestino(), 3); // calculamos los k caminos mas cortos entre el origen y el fin. Con k=3
                        
                        for(int a=0; a<RSA.size();a++){ // para cada algoritmo seleccionado
                            String algoritmoSeleccionado = RSA.get(a);
                            switch(algoritmoSeleccionado){ // vemos cual algoritmo ha sido selecciondo, y realizamos la ejecucion de cada uno
                                                           // en su correspondiente Topologia
                                case "MPSC":
                                    r=Algoritmos.MPSC_Algorithm(G[0],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[0],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                case "MTLSC":
                                    r=Algoritmos.MTLSC_Algorithm(G[1], d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[1],d);
                                    }  
                                     else{
                                        contB[a]++;
                                    }
                                    break;
                                case "FAR - EF":
                                    r=Algoritmos.FAR_EF_Algorithm(G[4],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[4],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                case "FAR - FF":
                                    r=Algoritmos.KSP_FF_Algorithm(G[2],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[2],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                    case "FAR - LF":
                                    r=Algoritmos.KSP_FF_Algorithm(G[7],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[7],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                case "FAR - RF":
                                    r=Algoritmos.KSP_RF_Algorithm(G[3],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[3],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                case "FAR - LU":
                                    r=Algoritmos.KSP_LU_Algorithm(G[5],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[5],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                case "FAR - MU":
                                    r=Algoritmos.KSP_MU_Algorithm(G[5],d,ksp,capacidadE);
                                    if(r!=null){
                                        Utilitarios.asignarFS(ksp,r,G[5],d);
                                    }  
                                    else{
                                        contB[a]++;
                                    }
                                    break;
                                
                                }
                            
                            }
                        contD++; 
                    }
                     // para cada Algoritmo y su correspondiente topologia, disminuimos el tiempo de permanecia
                    for(int a=0;a<RSA.size();a++){
                        if(RSA.get(a)=="MPSC"){
                            Utilitarios.Disminuir(G[0]);
                        }
                        else if(RSA.get(a)=="MTLSC"){
                            Utilitarios.Disminuir(G[1]);
                        }
                        else if(RSA.get(a)=="FAR - FF"){
                            Utilitarios.Disminuir(G[2]);
                        }
                        else if(RSA.get(a)=="FAR - LF"){
                            Utilitarios.Disminuir(G[7]);
                        }
                        else if(RSA.get(a)=="FAR - RF"){
                            Utilitarios.Disminuir(G[3]);
                        }
                        else if(RSA.get(a)=="FAR - EF"){
                            Utilitarios.Disminuir(G[4]);
                        }
                        else if(RSA.get(a)=="FAR - LU"){
                            Utilitarios.Disminuir(G[5]);
                        }
                        else if(RSA.get(a)=="FAR - MU"){
                            Utilitarios.Disminuir(G[6]);
                        }
                    }  
                }
            ++k;
            // almacenamos la probablidad de bloqueo final para cada algoritmo
            for(int a=0;a<RSA.size();a++){
                prob[a].add(((double)contB[a]/contD));
               System.out.println("Probabilidad: "+(double)prob[a].get(k)+" Algoritmo: "+a+" Earlang: "+earlang);
            }
            // avanzamos a la siguiente carga de trafico
            earlang+=paso;
        }
        this.etiquetaError.setText("Simulacion Terminada...");
        // una vez finalizado, graficamos el resultado.
        Utilitarios.GraficarResultado(prob, this.panelResultado, this.etiquetaResultado,RSA,paso);
        String demandasTotales=""+contD; // mostramos la cantidad de demandas totales recibidas
        this.etiquetaDemandasTotales.setText(demandasTotales);
        }else{ // control de errores posibles realizados al no completar los parametros de simulacion
            if(listaDemandas.getSelectedIndex()<0){
               mensajeError=mensajeError+"Demanda";
                
            }
            if (this.listaRSA.getSelectedIndex()<0){
                if(mensajeError=="Seleccione "){
                    mensajeError=mensajeError+"Algoritmo RSA";
                }else{
                    mensajeError=mensajeError+", Algoritmo RSA";
                }
            }
            if(this.listaRedes.getSelectedIndex()<0){
                if(mensajeError=="Seleccione "){
                    mensajeError=mensajeError+"Topologia";
                }else{
                    mensajeError=mensajeError+", Topologia";
                }   
            }
            if(mensajeError!="Seleccione "){
                this.etiquetaError.setText(mensajeError);
            }
        }
    }//GEN-LAST:event_botonEjecutarSimulacionActionPerformed

    private void listaRedesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaRedesMouseClicked
        if(this.listaRedes.getSelectedIndex()>=0){
            this.botonCambiarDatosdelosEnlaces.setEnabled(true);
            ImageIcon Img = new ImageIcon(getClass().getResource("Imagenes/"+(listaRedes.getSelectedValue()+".png")));
            etiquetaImagen.setIcon(Img);
            etiquetaImagen.setBounds(210, 100, 320, 170);
            etiquetaImagen.setVisible(true);
            etiquetaImagen.setOpaque(false);
            String redseleccionada=this.listaRedes.getSelectedValue();
            switch(redseleccionada){
                case "Red 0":
                    this.spinnerEspectroporEnlace.setValue((int)(this.Redes.getRed(0).getCapacidadTotal()/this.Redes.getRed(0).getAnchoFS()));
                    this.spinnerAnchoFS.setValue(this.Redes.getRed(0).getAnchoFS());
                    break;
                case "Red 1":
                    this.spinnerEspectroporEnlace.setValue((int)(this.Redes.getRed(1).getCapacidadTotal()/this.Redes.getRed(1).getAnchoFS()));
                    this.spinnerAnchoFS.setValue(this.Redes.getRed(1).getAnchoFS());
                    break;
                case "Red 2":
                    this.spinnerEspectroporEnlace.setValue((int)(this.Redes.getRed(2).getCapacidadTotal()/this.Redes.getRed(2).getAnchoFS()));
                    this.spinnerAnchoFS.setValue(this.Redes.getRed(2).getAnchoFS());
                    break;
            }
        }
    }//GEN-LAST:event_listaRedesMouseClicked

    private void listaEnlacesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaEnlacesMouseClicked
        int ban=0;
        int i=0;
        int j=0;
        if(this.listaEnlaces.getSelectedIndex()>=0){
            String enlaceSeleccionado = this.listaEnlaces.getSelectedValue();
            this.PaneleEditarEnalce.setVisible(true);
            this.etiquetaEditarEnlace.setText(enlaceSeleccionado);
            if(enlaceSeleccionado.substring(9,10).equals(" ")){
                i=Character.getNumericValue(enlaceSeleccionado.charAt(8));
                ban=1;
            }else{
                i=Integer.parseInt(enlaceSeleccionado.substring(8, 10));
            }
            
            if(enlaceSeleccionado.length()>12){
                if(ban==1)
                   j=Integer.parseInt(enlaceSeleccionado.substring(12, enlaceSeleccionado.length()));
                else
                   j=Integer.parseInt(enlaceSeleccionado.substring(13, enlaceSeleccionado.length()));
            }
            else{
                j=Character.getNumericValue(enlaceSeleccionado.charAt(12));
            }
            this.etiquetaEspectro.setText("Espectro :");
            this.etiquetaAnchoFS.setText("Ancho del FS :");
            String red = this.listaRedes.getSelectedValue();
            switch(red){
                case "Red 0":
                    this.campoDeTextoEspectro.setText(""+this.Redes.getRed(0).acceder(i, j).getEspectro());
                    this.campoDeTextoAnchoFS.setText(""+this.Redes.getRed(0).acceder(i, j).getAnchoFS());
                    break;
                case "Red 1":
                    this.campoDeTextoEspectro.setText(""+this.Redes.getRed(1).acceder(i,j).getEspectro());
                    this.campoDeTextoAnchoFS.setText(""+this.Redes.getRed(1).acceder(i,j).getAnchoFS());
                    break;
                case "Red 2":
                    this.campoDeTextoEspectro.setText(""+this.Redes.getRed(2).acceder(i,j).getEspectro());
                    this.campoDeTextoAnchoFS.setText(""+this.Redes.getRed(2).acceder(i,j).getAnchoFS());
                    break;    
            }
        }
            
    }//GEN-LAST:event_listaEnlacesMouseClicked

    private void bGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bGuardarActionPerformed
        String abc="0123456789";
        String abc2="0123456789.";
        int i;
        int j;
        int ban=0;
        String enlaceSeleccionado = this.listaEnlaces.getSelectedValue();
        String error="Inserte :";
        int redS=0;
        int esp=0;
        double anch=0;
        switch(this.listaRedes.getSelectedValue()){
            case "Red 0":
                redS=0;
                break;
            case "Red 1":
                redS=1;
                break;
            case "Red 2":
                redS=2;
                break;
                
        }
        String espectro=this.campoDeTextoEspectro.getText();
        String anchoFS=this.campoDeTextoAnchoFS.getText();
        if(Utilitarios.verificarNumero(abc,espectro)){
            esp=1;
        }else{
            error+="-el espectro";
        }
        if(Utilitarios.verificarNumero(abc2,anchoFS)){
            anch=1;
        }else{
            error+="-el anchoFS";
        }
        if(error!="Inserte :"){
            if(error.equals("Inserte :-el espectro") || error.equals("Inserte :-el anchoFS"))
                error+=" correcto.";
            else
                error+=" correctos.";
            this.etiquetaErrorEditarEnlace.setText(error);
        }else{
            if(enlaceSeleccionado.substring(9,10).equals(" ")){
                i=Character.getNumericValue(enlaceSeleccionado.charAt(8));
                ban=1;
            }else{
                i=Integer.parseInt(enlaceSeleccionado.substring(8, 10));
            }
            
            if(enlaceSeleccionado.length()>12){
                if(ban==1)
                   j=Integer.parseInt(enlaceSeleccionado.substring(12, enlaceSeleccionado.length()));
                else
                   j=Integer.parseInt(enlaceSeleccionado.substring(13, enlaceSeleccionado.length()));
            }
            else{
                j=Character.getNumericValue(enlaceSeleccionado.charAt(12));
            }
       
            esp=Integer.parseInt(espectro);
            anch=Double.parseDouble(anchoFS);
            if(anch>esp){
                this.etiquetaErrorEditarEnlace.setText("Error: Ancho del FS mayor al Espectro.");
            }else{
                this.Redes.getRed(redS).acceder(i,j).setEspectro(esp);
                this.Redes.getRed(redS).acceder(i,j).setAnchoFS(anch);
                this.Redes.getRed(redS).acceder(j,i).setEspectro(esp);
                this.Redes.getRed(redS).acceder(j,i).setAnchoFS(anch);
                this.etiquetaErrorEditarEnlace.setText("Se ha guardado correctamente.");
            } 
        }
    }//GEN-LAST:event_bGuardarActionPerformed

    private void botonCerrarCerrarDatosEnlacesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrarCerrarDatosEnlacesActionPerformed
        this.panelCambiarDatosdelosEnlaces.setVisible(false);
        this.etiquetaErrorEditarEnlace.setText("");
    }//GEN-LAST:event_botonCerrarCerrarDatosEnlacesActionPerformed

    private void botonCerrarEditarEnlaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrarEditarEnlaceActionPerformed
        this.PaneleEditarEnalce.setVisible(false);
        this.etiquetaErrorEditarEnlace.setText("");
    }//GEN-LAST:event_botonCerrarEditarEnlaceActionPerformed

    private void spinnerFactorCapacidadStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerFactorCapacidadStateChanged
        // TODO add your handling code here:
        double valor=(int)this.spinnerFactorCapacidad.getValue();
        valor=valor*12.5;
        this.etiquetaValorGHz.setText(" "+valor+" ");
    }//GEN-LAST:event_spinnerFactorCapacidadStateChanged

    private void botonCambiarDatosdelosEnlacesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCambiarDatosdelosEnlacesActionPerformed
        DefaultListModel modelo = new DefaultListModel();
        GrafoMatriz G=new GrafoMatriz();
        this.listaEnlaces.setModel(modelo);
        String red = this.listaRedes.getSelectedValue();
        switch(red){
            case "Red 0":
            G=this.Redes.getRed(0);
            break;
            case "Red 1":
            G=this.Redes.getRed(1);
            break;
            case "Red 2":
            G=this.Redes.getRed(2);
            break;
        }
        for(int i=0;i<G.getCantidadDeVertices();i++){
            for(int j=i;j<G.getCantidadDeVertices();j++){
                if(G.acceder(i, j)!=null){
                    modelo.addElement(" Enlace "+i+" - "+j);
                }
            }
        }
        this.listaEnlaces.setModel(modelo);
        this.panelCambiarDatosdelosEnlaces.setVisible(true);

    }//GEN-LAST:event_botonCambiarDatosdelosEnlacesActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PaneleEditarEnalce;
    private javax.swing.JButton bGuardar;
    private javax.swing.JButton botonCambiarDatosdelosEnlaces;
    private javax.swing.JButton botonCerrarCerrarDatosEnlaces;
    private javax.swing.JButton botonCerrarEditarEnlace;
    private javax.swing.JButton botonEjecutarSimulacion;
    private javax.swing.JTextField campoDeTextoAnchoFS;
    private javax.swing.JTextField campoDeTextoEspectro;
    private javax.swing.JLabel etiqueta1;
    private javax.swing.JLabel etiqueta2;
    private javax.swing.JLabel etiquetaAnchoFS;
    private javax.swing.JLabel etiquetaAnchoFSActual;
    private javax.swing.JLabel etiquetaCapacidadActual;
    private javax.swing.JLabel etiquetaDemanda;
    private javax.swing.JLabel etiquetaDemandasTotales;
    private javax.swing.JLabel etiquetaEditarEnlace;
    private javax.swing.JLabel etiquetaError;
    private javax.swing.JLabel etiquetaErrorEditarEnlace;
    private javax.swing.JLabel etiquetaEspectro;
    private javax.swing.JLabel etiquetaFactorTiempo;
    private javax.swing.JLabel etiquetaGHz;
    private javax.swing.JLabel etiquetaImagen;
    private javax.swing.JLabel etiquetaRSA;
    private javax.swing.JLabel etiquetaResultado;
    private javax.swing.JLabel etiquetaTextoDemandasTotales;
    private javax.swing.JLabel etiquetaTiempoActual;
    private javax.swing.JLabel etiquetaTituloDeEnlaces;
    private javax.swing.JLabel etiquetaTopologia;
    private javax.swing.JLabel etiquetaValorGHz;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JList<String> listaDemandas;
    private javax.swing.JList<String> listaEnlaces;
    private javax.swing.JList<String> listaRSA;
    private javax.swing.JList<String> listaRedes;
    private javax.swing.JPanel panelCambiarDatosdelosEnlaces;
    private javax.swing.JPanel panelResultado;
    private javax.swing.JSpinner spinnerAnchoFS;
    private javax.swing.JSpinner spinnerEarlang;
    private javax.swing.JSpinner spinnerEspectroporEnlace;
    private javax.swing.JSpinner spinnerFactorCapacidad;
    private javax.swing.JSpinner spinnerFactorTiempo;
    private javax.swing.JSpinner spinnerPaso;
    private javax.swing.JSpinner spinnerTiempoSimulacion;
    // End of variables declaration//GEN-END:variables
  
   
}

