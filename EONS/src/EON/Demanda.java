
package EON;

import java.util.Random;

/**
 *
 * @author Team Delvalle
 * 
 * La clase se encarga de generar las Demandas.
 * El origen, el destino de forma aleatoria.
 * Y alacenar el numero de FSs requeridos y el tiempo de permanencia en la red.
 */
public class Demanda {
    
    private int origen;
    private int destino;
    private int nroFS;
    private int tiempo;
    
    public Demanda(int o,int d, int n,int t){
        this.origen=o;
        this.destino=d;
        this.nroFS=n;
        this.tiempo=t;
    }
    public Demanda(){
        origen=destino=nroFS=0;
    }
    public int getNroFS(){
        return this.nroFS;
    }
    public int getOrigen(){
        return this.origen;
    }
    public int getDestino(){
        return this.destino;
    }
    public int getTiempo(){
        return this.tiempo;
    }
    public void obtenerDemandas(int capacidad, int tiempo,int n){
        Random r=new Random();
        this.origen=r.nextInt(n);
        this.destino=r.nextInt(n);
        while(this.destino==this.origen)
            this.destino=r.nextInt(n);
        this.nroFS=capacidad;
        this.tiempo=tiempo;
    }
}
