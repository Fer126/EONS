
package EON.Utilitarios;

import EON.*;
import java.util.List;
import java.util.Random;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Team Delvalle
 */
public class Utilitarios {
    /**************************************************************
    *                                                             *
    *Algoritmos utilizados para realizar los K caminos mas cortos**
    *                                                             *
    ***************************************************************/
    /*Algoritmo de los k caminos mas cortos*/
    public static ListaEnlazada [] KSP(GrafoMatriz G, int s, int d, int k){
        
        ListaEnlazada A[]= new ListaEnlazada[k];
        A[0]=Dijkstra(G,s,d);
        ListaEnlazada B[]= new ListaEnlazada[100];
        
        Nodo spurNode=new Nodo();
        ListaEnlazada spurPath,rootPath,totalPath;
        
        int [][]enlacesR=new int [100][2];
        int nodosR[] = new int[100];
        int cont=0;
        int b=-1;
        int flag;
        for(int j=1;j<k;j++){
            for(int i=0;i<A[j-1].getTamanho()-2;i++){
                spurPath=new ListaEnlazada();
                rootPath=new ListaEnlazada();
                totalPath=new ListaEnlazada();
                flag=0;
                spurNode=A[j-1].nodo(i);
                rootPath=A[j-1].optenerSublista(0, i);
                int h=0;
                //Elimnar enlaces
                cont=0;
                while(A[h]!=null){
                    ListaEnlazada p=A[h];
                    if(i<p.getTamanho()-1){
                        if(rootPath.comparar(p.optenerSublista(0, i))){
                            G.acceder(p.nodo(i).getDato(), p.nodo(i+1).getDato()).setEstado(false);
                            enlacesR[cont][0]=p.nodo(i).getDato();
                            enlacesR[cont][1]=p.nodo(i+1).getDato();
                            cont++;
                        }
                    }
                    h++;
                }
                //Eliminar Nodos
                h=0;
                int cont3=0;
                while(h<rootPath.getTamanho()){
                    if(rootPath.nodo(h).getDato()!=spurNode.getDato()){
                        for(int cont2=0;cont2<G.getCantidadDeVertices();cont2++){
                            if(G.acceder(rootPath.nodo(h).getDato(),cont2)!=null ){
                                G.acceder(rootPath.nodo(h).getDato(),cont2).setEstado(false);
                            }
                        }
                        for(int cont2=0;cont2<G.getCantidadDeVertices();cont2++){
                             if(G.acceder(cont2,rootPath.nodo(h).getDato())!=null){
                                 G.acceder(cont2,rootPath.nodo(h).getDato()).setEstado(false);
                            }
                        }
                        nodosR[cont3]=rootPath.nodo(h).getDato();
                        cont3++;
                    }  
                    h++;
                }
                //Ver si el spurnode tiene enlaces libres para ir hasta el destino.
                for(int cont2=0;cont2<G.getCantidadDeVertices();cont2++){
                    if(G.acceder(spurNode.getDato(), cont2)!=null && G.acceder(spurNode.getDato(), cont2).getEstado()==true){
                        flag=1;
                        break;
                    }
                }
                if(flag==1){
                    spurPath=Dijkstra(G,spurNode.getDato(),d);
                    //Camino potencial
                    if(spurPath!=null){
                        totalPath.union(rootPath,spurPath);
                        //Calcular peso del camino potencial
                        if(rootPath.getTamanho()!=1){
                            int peso=0;
                            for(int cont2=0;cont2<rootPath.getTamanho()-1;cont2++){
                                peso=peso+G.acceder(rootPath.nodo(cont2).getDato(),rootPath.nodo(cont2+1).getDato()).getDistancia();
                            }
                            totalPath.nodo(totalPath.getTamanho()-1).setDato(peso+spurPath.nodo(spurPath.getTamanho()-1).getDato());   
                        }
                        if(verificar(B,b,totalPath)){
                            b++;
                            B[b]=totalPath;
                        }
                    }                
                }
                reestablecerNodosEnlaces(G,enlacesR,cont,nodosR,cont3);
            }
            if(b>-1){
                ordenar(B,b);
                A[j]=B[0];
                for(int cont2=0;cont2<b;cont2++){
                    B[cont2]=B[cont2+1];
                }
                b--;
            } else{
               // System.out.println("\nSolo existen "+j+" caminos");
                break;
            }       
        }
        return A;
    }
    
    /*Algoritmo encargado de retornar el resultado del algoritmo de Dijkstra 
      en una lista enlazada con los nodos del camino mas corto.*/
    public static ListaEnlazada Dijkstra(GrafoMatriz G,int o,int d){
        int aux;
        Tabla t = new Tabla(G.getCantidadDeVertices());
        t.setDistancia(o,0);
        boolean camino=DM(G,t,o,d);
        if(camino){
            ListaEnlazada l = new ListaEnlazada();
            aux=d;
            while(aux!=o){
                l.insertarAlComienzo(aux); 
                aux=t.getOrigen(aux);
            }
            l.insertarAlComienzo(o);
            l.insertarAlfinal(t.getDistancia(d));
            return l;
        }
        return null;
    }
    
    /*Algoritmo de Disjkstra)*/
    public static boolean DM(GrafoMatriz G, Tabla t, int v,int d){
        if(v!=-1 && v!=d){
            if(!t.getMarca(v)){     
                t.marcar(v);
                for(int i=0; i<G.getCantidadDeVertices();i++){
                    if(!t.getMarca(i) && G.acceder(v, i)!=null && G.acceder(v, i).getEstado()==true){
                        if(t.getDistancia(i)==-1){
                            t.setDistancia(i,t.getDistancia(v)+G.acceder(v, i).getDistancia());
                            t.setOrigen(i, v);
                        }else{
                            if((t.getDistancia(v)+G.acceder(v, i).getDistancia())<t.getDistancia(i)){
                                t.setDistancia(i,t.getDistancia(v)+G.acceder(v, i).getDistancia());
                                t.setOrigen(i, v);
                            }
                        }
                    }
                } 
            }
        }else if(v!=-1 && v==d){
            t.marcar(v);
            return true;
        }
        else if(v==-1){
            return false;
        }
        return DM(G,t,t.menor(),d); 
    }

    /*Restablecer los nodos eliminados por el Algoritmo KSP*/
    public static void reestablecerNodosEnlaces(GrafoMatriz G,int [][]e,int n1,int []nodos,int n2){
        for(int i=0;i<n1;i++){
            G.acceder(e[i][0], e[i][1]).setEstado(true);
        }
        for(int i=0;i<n2;i++){
            for(int j=0;j<G.getCantidadDeVertices();j++){
                if(G.acceder(nodos[i],j)!=null)
                    G.acceder(nodos[i],j).setEstado(true);
            }
            for(int j=0;j<G.getCantidadDeVertices();j++){
                if(G.acceder(j,nodos[i])!=null)
                    G.acceder(j,nodos[i]).setEstado(true);
            }
        }
    }
    /*Ordenar una lista enlazada de n elementos*/
    
    
    /****************************************************************
     *
     *****************Algoritmos de Utilizacion general**************
     *
     ****************************************************************/
    /*Algoritmo para ordenar una lista enalzada de n elementos*/
    public static void ordenar(ListaEnlazada [] v,int n){
        ListaEnlazada aux=new ListaEnlazada();
        for(int i=0;i<=n-1;i++){
            for(int j=i+1;j<=n;j++){
                if(v[i].nodo(v[i].getTamanho()-1).getDato()>v[j].nodo(v[j].getTamanho()-1).getDato()){
                    aux=v[i];
                    v[i]=v[j];
                    v[j]=aux;
                }
            }
        }
    }
    /*Algoritmo para verificar si dos listas son iguales*/
    public static boolean verificar(ListaEnlazada [] b,int n,ListaEnlazada v){
        Nodo aux=new Nodo();
        boolean ban;
        for(int i=0;i<=n;i++){
            aux=b[i].getInicio();
            if(b[i].getTamanho()==v.getTamanho()){
                ban=true;
                for(Nodo j=v.getInicio();j!=null;j=j.getSiguiente(),aux=aux.getSiguiente()){
                    if(j.getDato()!=aux.getDato()){
                        ban=false;
                        break;
                    }
                }
                if(ban)
                    return false;
            }
        }
        return true;
    }
    /*Verificar si el String 'dato' es un numero*/
    public static boolean verificarNumero(String abc,String dato){
        boolean ban=false;
        for(int i=0;i<dato.length();i++){
            ban=false;
            for(int j=0;j<abc.length();j++){
               if(dato.charAt(i)==abc.charAt(j)){
                  ban=true;
                  break;
                }
            }   
            if(!ban){
                break;
            }
        }
        if(dato=="0"){
            ban=false;
        }
        return ban; 
    }
    
       
    /*************************************************************************
     * 
     * Algortimos Utilizados para el calculo de las Demandas/Unidad de tiempo
     * 
     *************************************************************************/
    /*Calcular el valor de lambda en funcion al Earlang y el tiempo*/
    public static double demandasporTiempo(int E, int t){
        return (double)E/t;
    }
    /*Del resultado del Algoritmo demandasporTiempo(), se calcula la cantidad  
    * segura de demandas en una unidad de tiempo
    */
    public static double [] cantidadDemandas(double l){
        
        int entero=(int)l;
        double decimal=(double)(l-entero);
        double [] r = new double [2];
        r[0]=entero;
        r[1]=decimal;
        return r;
        
    }
    /*Toma la parte decimal optenida del Algoritmo demandasporTiempo()
    * y retorna si se envia una demanda mas pàra una unidad de tiempo
    */
    public static boolean probabilidadUnaDemanda(double d){
        
        int n=(int)(d*100);
        Random nro= new Random();
        int r=1+nro.nextInt(99);
        if(r>n)
            return false;
        return true;
        
    }
    
    /*
    * Demandas totales que caeran segun la carga de trafico: erlang, y el tiempo
    * de duracion de las demandas: t
    */
    public static int demandasTotalesPorTiempo(int erlang,int t){
        
         double demandasPorUnidadTiempo;
         double [] demanda;
         int d;
         demandasPorUnidadTiempo=demandasporTiempo(erlang,t);
         demanda=cantidadDemandas(demandasPorUnidadTiempo);
         d=(int)demanda[0];
         if(Utilitarios.probabilidadUnaDemanda(demanda[1])){
                        d++;
         }
         return d;
               
    }
    
    /*Algoritmo que se encarga de asignar a una demanda los FSs requeridos en la red*/
    public static void asignarFS(ListaEnlazada ksp[],Resultado r,GrafoMatriz G,Demanda d){
        int util=0;
        int cont=0;
        for(Nodo nod=ksp[r.getCamino()].getInicio();nod.getSiguiente().getSiguiente()!=null;nod=nod.getSiguiente()){
            for(int p=r.getInicio();cont<=d.getNroFS() && p<=r.getFin();p++){
                cont++;
                G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getFS()[p].setEstado(0);
                G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getFS()[p].setTiempo(d.getTiempo());
                util= G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getUtilizacion()[p]++;
                G.acceder(nod.getDato(), nod.getSiguiente().getDato()).setUtilizacionFS(p,util);
                G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getFS()[p].setEstado(0);
                G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getFS()[p].setTiempo(d.getTiempo());
                util=G.acceder(nod.getSiguiente().getDato(), nod.getDato()).getUtilizacion()[p]++;
                G.acceder(nod.getSiguiente().getDato(), nod.getDato()).setUtilizacionFS(p,util);
            }   
        }
    }
    
    /*Algotimo que se encarga de graficar el resultado final de las problidades de bloqueo con respecto al earlang*/
    public static void GraficarResultado(List result[],JPanel panelResultado, JLabel etiquetaResultado, List<String> lista,int paso){
        double sum=1;
        XYSplineRenderer renderer=new XYSplineRenderer();
        XYSeries series[]=new XYSeries[result.length];
        XYSeriesCollection datos=new  XYSeriesCollection();
        ValueAxis ejex=new NumberAxis();
        ValueAxis ejey=new NumberAxis();
        XYPlot plot;
        panelResultado.removeAll();
        for(int i=0;i<result.length;i++){
            series[i]= new XYSeries((String)lista.get(i));
            for(int j=0;j<result[i].size();j++){
                sum+=paso;
                series[i].add(sum, (double)result[i].get(j));   
            }
            sum=1;
            datos.addSeries(series[i]);
        }
        
        ejex.setLabel("Erlang");
        ejey.setLabel("Probalididad de bloqueo(%)");
        plot =new XYPlot(datos,ejex,ejey,renderer);
        JFreeChart grafica = new JFreeChart(plot);
        //grafica.setTitle("Probabilidad de Bloqueo");
        ChartPanel panel= new ChartPanel(grafica);
        panel.setBounds(2, 2, 466, 268);
        panelResultado.add(panel);
        panelResultado.repaint();
        panelResultado.setVisible(true);
    }
    /*El algoritmo que se encarga en cada unidad de tiempo de disminuir el tiempo de permanecia en la red de los
    FSs.*/
    public static void Disminuir(GrafoMatriz G){
        for(int i=0;i<G.getCantidadDeVertices();i++){
            for(int j=0;j<G.getCantidadDeVertices();j++){
                if(G.acceder(i, j)!=null){
                    for(int k=0;k<G.acceder(i, j).getFS().length;k++){
                        if(G.acceder(i, j).getFS()[k].getEstado()==0){
                            G.acceder(i, j).getFS()[k].setTiempo(G.acceder(i, j).getFS()[k].getTiempo()-1);
                            if(G.acceder(i, j).getFS()[k].getTiempo()==0){
                                G.acceder(i, j).getFS()[k].setEstado(1);
                            }
                        }
                    }
                }
            }
        }
    }
    /*
    *Genera un alista de n numeros enteros aleatrios todos diferentes.
    * Parametros: int n: Cantidad de numeros a generar.
    * Retorna: int [] : lista de numeros generados
    * Rango de numeros aleatorios [ 0 , n ).
    */
    public static int [] listaDeNumeros(int n){
        
        /*Definicion de variables.*/
        
        Random r = new Random(); //Objeto a generar los numeros aleatorios
        int nro; // variable que almacenara cada numero aleatorio a generar.
        int [] lista=new int[n]; // lista que almacenara los n numeros aleatorios.
        int flag1=1; // Variable bandera que identifica si se encontro un numero aleatorio diferente
                     // a todos los anteriormente generados.
                     
        /*Comprobamos si cada numero gnerado es diferente a todos los demas numeros ya generados.*/
        int i=0;
        while(i<n){
            nro=r.nextInt(n);// Generamos un numero
            flag1=1;
            for(int j=0;j<i;j++){
                if(lista[j]==nro){ // si es igual a un anterior, lo desechamos.
                    flag1=0;
                    break;
                }
            }
            if(flag1==1){ // si no se cambio de valor la badera, es un nuemro valido y lo almacenamos.
                lista[i]=nro;
                i++;
            }
        }
        return lista;
    } 
}
