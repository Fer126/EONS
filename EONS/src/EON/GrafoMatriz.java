package EON;
/**
 *
 * @author Team Delvalle
 * El grafo en su forma de matriz de adyacencia para representar la topologia de una red.
 * Almacena:
 * Los datos de los enlaces para cada vertice i,j 
 * La cantidad de vertices 
 * La capacidad total que es la catidad de FSs disponibles por enlace en la red.
 * El ancho de cada FS.
 */
public class GrafoMatriz {
    
    private Enlace [][] vertices;
    private boolean [] marcas;
    private int cantidadVertices;
    private int capacidadTotal;
    private double anchoFS;
    
    public GrafoMatriz(int V){
      
      this.marcas = new boolean[V];
      this.vertices = new Enlace[V][V];
      this.cantidadVertices=V;
      this.capacidadTotal=0;
      this.anchoFS=0;
    
    }
    public GrafoMatriz(){
        
        this.cantidadVertices=0;
        this.marcas=null;
        this.vertices=null;
    }
    
    public GrafoMatriz insertarDatos(double [][][]v){
        double nro=0,distancia=0,espectro=0,ancho=0;
        
        for(int i=0;i<this.vertices.length;i++){
            for(int j=0;j<this.vertices.length;j++){
                if(v[i][j][1]!=0){
                    nro=v[i][j][0];
                    distancia=v[i][j][1];
                    espectro=v[i][j][2];
                    this.capacidadTotal=(int)espectro;
                    ancho=v[i][j][3];
                    this.anchoFS=ancho;
                    this.vertices[i][(int)nro]=new Enlace(i,(int)nro,(int)espectro,(int)distancia,ancho);
                    this.vertices[(int)nro][i]=new Enlace((int)nro,i,(int)espectro,(int)distancia,ancho);
                 }
            }
        }
       return this;
    }
    public void marcar(int i){
        marcas[i]=true;
    }
    public boolean getMarca(int i){
        return marcas[i];
    }
    public Enlace acceder(int i, int j){
        return this.vertices[i][j];
    }
    public int getCantidadDeVertices(){
        return this.cantidadVertices;
    }
    public int getCapacidadTotal(){
        return this.capacidadTotal;
    }
    public double getAnchoFS(){
        return this.anchoFS;
    }
    public void setCapacidadTotal(int c){
        this.capacidadTotal=c;
         for(int i=0;i<this.vertices.length;i++){
            for(int j=0;j<this.vertices.length;j++){
                if(this.acceder(i, j)!=null){
                    this.acceder(i, j).setEspectro(c);
                }
            }
        }   
    }
    public void setAnchoFS(double a){
        this.anchoFS=a;
        for(int i=0;i<this.vertices.length;i++){
            for(int j=0;j<this.vertices.length;j++){
                if(this.acceder(i, j)!=null){
                    this.acceder(i, j).setAnchoFS(a);
                }
            }
        }
    }
    public  void restablecerFS(){
        for(int i=0;i<this.getCantidadDeVertices();i++){
            for(int j=0;j<this.getCantidadDeVertices();j++){
                if(this.acceder(i, j)!=null){
                    for(int k=0;k<this.acceder(i, j).getFS().length;k++){
                        this.acceder(i, j).getFS()[k].setEstado(1);
                        this.acceder(i,j).getFS()[k].setTiempo(0);
                    }
                }
            }
        }
    }
}
